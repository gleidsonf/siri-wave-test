import { Component, OnInit } from '@angular/core';
import SiriWave from 'siriwave/dist/siriwave';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'siriwave-test';
  siri;
  status: String = '';

  ngOnInit() {
  this.siri = new SiriWave({
      container: document.getElementById('siri-container'),
      width: 640,
      height: 200,
      style: 'ios9',
      cover: true,
      autostart: true,
    });
    console.log(this.siri);
    this.status = 'Started';
  }

  onStart() {
    this.siri.start();
    this.status = 'Started';
  }

  onStop() {
    this.siri.stop();
    this.status = 'Stoped';
  }

  setSpeed(event) {
    this.siri.setSpeed(event.target.value);
  }

  setAmplitude(event) {
    this.siri.setAmplitude(event.target.value);
  }

}
